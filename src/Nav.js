import React from 'react'
import './App.css';
import { Link } from 'react-router-dom'

function Nav() {
  const link = {
    color: 'white',
    textDecoration: 'none'
  }

  return (
    <nav>
      <Link style={link} to="/">
        <h3>Logo</h3>
      </Link>
      <ul className="nav-links">
        <Link style={link} to="/about">
          <li>About</li>
        </Link>
        <Link style={link} to="/shop">
          <li>Shop</li>
        </Link>
      </ul>
    </nav>
  );
}

export default Nav;
