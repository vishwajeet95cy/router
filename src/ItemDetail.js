import React, { useState, useEffect } from 'react'
import './App.css';

function Item({ match }) {

  const style = {
    backgroundColor: 'black'
  }

  useEffect(() => {
    fetchItem();
    console.log(match);
  }, [])

  const [item, setItem] = useState({
    item: '',
    images: ''
  });

  const fetchItem = async () => {
    const fetchItem = await fetch(`https://fortnite-api.theapinetwork.com/item/get?id=${match.params.id}`)
    const item = await fetchItem.json()
    setItem(item.data.item)
  }


  return (
    <div className="App">
      <h1>{item.name}</h1>
      <h2>{item.costmeticId}</h2>
      <img src={item.images.icon} style={style} alt={item.name} width="150px" height="150px" />
      <p>{item.description}</p>
    </div>
  );
}

export default Item;
